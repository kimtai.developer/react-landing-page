import { useEffect } from "react";
import * as common from "./components/common"
import * as body from "./components/sections"
import Loader from "./components/partials/loader"
import AOS from 'aos';
import 'aos/dist/aos.css';

const  App = () => {

    useEffect( () => {
        AOS.init(
            {
                duration: 1000,
                once: true,
            }   
        );
    }, []);

    return (
        <>
            <common.Topbar />
            <common.Header />
            
            <body.Hero />

            <main id="main">
                <body.Features />
                <body.AboutUs />
                <body.Skills />
                <body.Counts />
                <body.Clients />
                <body.Services />
                <body.Testimonial />
                <body.Portfolio />
                <body.Team />
                <body.Pricing />
                <body.Faq />
                <body.Contact />
            </main>
            
            <common.Footer />

            <Loader />
        </>
    );
}

export default App;
