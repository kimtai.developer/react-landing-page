const Loader = () => {
    return (
        <>
            <div id="preloader"></div>
            <a href="#" className="back-to-top"><i className="icofont-simple-up"></i></a>
        </>
    )
}

export default Loader