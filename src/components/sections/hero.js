const Hero = () => {
    return (
        <section id="hero" className="d-flex align-items-center">
            <div className="container" data-aos="zoom-out" data-aos-delay="100">
                <h1>Welcome to <span>Mobo Inc</span></h1>
                <h2>We are team of talented designers making websites with React</h2>
                <div className="d-flex">
                    <a href="#about" className="btn-get-started scrollto">Get Started</a>
                    <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" className="venobox btn-watch-video" data-vbtype="video" data-autoplay="true"> Watch Video <i className="icofont-play-alt-2"></i></a>
                </div>
            </div>
        </section>
    )
}

export default Hero